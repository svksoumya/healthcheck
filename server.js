import express from 'express';
import fs from 'fs';

//P package.json to read the app name and version
var pjson = JSON.parse(fs.readFileSync('./package.json'));

//creating an instance of express server    
const app = express()
const port = 8080

//redirectly root path '/' to '/health'
app.get('/',(req,res) => {
    res.redirect('/health')
})
//handler for /health endpoint
app.get('/health',(req,res) => {
    //read git hash from git head
    var rev = fs.readFileSync('.git/HEAD').toString().trim();
    if (rev.indexOf(':') === -1) {
    } else {
        rev = fs.readFileSync('.git/' + rev.substring(5)).toString().trim();
    }
    
    //sending json response with git hash, version and app name
    res.send({
        "git_hash":rev,
        "app_name":pjson.name,
        "app_version":pjson.version
    })
})

//starting the app server at port 8080
app.listen(port, () => {
    console.log('Application Started')
})
