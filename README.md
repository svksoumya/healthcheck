## **Prerequisites**

- Node JS
- Docker Desktop (Kubernets enabled)

## **Steps to build and run the API in your local machine**

- Download or clone the repo
- Open terminal pointing to the project folder
- run the command `npm install` this will Download all the project dependencies
- run the command `node server.js` this will start server app running on port 8080
- open browser and navigate to endpoint [http://localhost:8080](localhost)
- this will redirect to the /health endpoint which display the app name, app version and git hash as below
-![](images/local-url-browser.PNG)

## **Steps to build and run the API from docker image**

- run the below command to pull the docker image from your terminal
  `docker pull registry.gitlab.com/svksoumya/healthcheck/image:latest`
- run the below command to spun up the docker container, ports mapping can be changed if you want to run on any other ports
  `docker run -d -p 8080:8080 registry.gitlab.com/svksoumya/healthcheck/image:latest`
- open browser and navigate to endpoint [http://localhost:8080](localhost)
- this will redirect to the /health endpoint which display the app name, app version and git hash as below
-![](images/local-url-browser-8081.PNG)

## **Steps to build and run on a Kubernets cluster**

- Deploy to Kubernets cluster using the manifest file app-deployment.yaml as below  
  `kubectl apply -f "https://gitlab.com/svksoumya/healthcheck/-/raw/master/app-deployment.yaml"` 
-![](images/deploy_kubectl_manifest.PNG)
- check if the cluster is up and running with the 2 nodes as specified in the yaml 
  `kubectl get pods` 
-![](images/check_node_health.PNG)
- check the files including package.json is copied from the image to the cluster 
  `kubectl exec healthcheckapp-deployment-74bcdcd85f-68q7c ls`
-![](images/check_dependency_files.PNG)
- check the output of localhost:8080/health from the container" 
  `kubectl exec healthcheckapp-deployment-74bcdcd85f-68q7c curl -i 'http://localhost:8080/health'`
-![](images/start_app-check_output.PNG)

### Above steps are tested using kubernetes.io interactive tutorial
